# Horizon Europe Guidance

[![](https://gitlab.tudelft.nl/tud-data-stewards/horizon-europe-guidance/-/badges/release.svg)](https://gitlab.tudelft.nl/tud-data-stewards/horizon-europe-guidance)

[Wiki](https://gitlab.tudelft.nl/data-stewards/horizon-europe-guidance/-/wikis/home) for Horizon Europe Guidance

## Description

This is the git repository for the [wiki](https://gitlab.tudelft.nl/data-stewards/horizon-europe-guidance/-/wikis/home)
containing guidance and example answers for the Open Science & Research Data Management Sections of Horizon Europe Applications.

The document follows [Calendar Versioning](https://calver.org/) with a YYYY.0M.Micro format. This means that the YYYY.0M part
of the version will only change if major changes are made to the document, i.e., if Horizon Europe makes changes to their policies.

**How to use the guidance**

- If you are adapting (parts of) the example answers in the guidance for your grant application, you are free to do so without attribution. For all other purposes, a [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/) applies.
- Delete what is not applicable in the example answers, e.g. if the project doesn’t produce software, then ignore all text related to software
- Replace text in square brackets with that appropriate for your application, e.g. project will be made openly available through the \[4TU.ResearchData, zenodo, …\] repository.
- It's advisable to consult with your faculty [Data Steward](https://www.tudelft.nl/en/library/research-data-management/r/support/data-stewardship/contact) to obtain feedback and advice on the appropriateness of using the example answers for your specific situation.


## Support
<!--Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.-->

For support with a Horizon Europe proposal, contact your [faculty Data Steward](https://www.tudelft.nl/en/library/research-data-management/r/support/data-stewardship/contact).



## Contributing

<!-- For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
-->
If changes need to be made to the document, open an issue describing the need for modification.

Once a (major) change has been made, add it to the [CHANGELOG](./CHANGELOG). No need to document
small changes (e.g. typos).


## Authors and acknowledgment
<!--Show your appreciation to those who have contributed to the project.-->

TU Delft Data stewards

- Heather Andrews
- Lora Armstrong
- Bjørn Bartholdy
- Nicolas Dintzner
- Xinyan Fan
- Richard Grimes
- Santosh Ilamparuthi
- Jeff Love
- Arthur Newton
- Esther Plomp
- Deepshikha Purwar
- Janine Strandberg
- Yasemin Türkyilmaz-van der Velden
- Yan Wang

## License
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
